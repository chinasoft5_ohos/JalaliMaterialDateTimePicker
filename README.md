# JalaliMaterialDateTimePicker

#### 项目介绍
- 项目名称：JalaliMaterialDateTimePicker
- 所属系列：openharmony 第三方组件适配移植
- 功能：多样式的时间/日期选择器。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 0.1.2

#### 效果演示
![JalaliMaterialDateTimePicker_1](/screenshots/Screenshot.gif)

#### 安装教程

在moudle级别下的build.gradle文件中添加依赖 
```
// 添加maven仓库 
repositories {
   maven {
       url 'https://s01.oss.sonatype.org/content/repositories/releases/'
   }
}

// 添加依赖库
dependencies {  
    implementation 'com.gitee.chinasoft_ohos:JalaliMaterialDateTimePicker:1.0.0' 
}
```

在sdk6，DevEco Studio 2.2 Beta1项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
```
class MainAbility extends FractionAbility {
    private DatePickerDialog dpd;

    // 改变这里
 dateButton.setClickedListener(c -> {
            Calendar now = Calendar.getInstance();
            if (defaultSelection.isChecked()) {
                now.add(Calendar.DATE, 7);
            }
            // 语言切换
            if (selectedIndex[0] == 0) {
                calendarType = DatePickerDialog.Type.JALALI;
            } else {
                calendarType = DatePickerDialog.Type.GREGORIAN;
            }

            switch (calendarType) {
                case GREGORIAN:
                    now = Calendar.getInstance();
                    break;

                case JALALI:
                    now = JalaliCalendar.getInstance();
                    break;
            }
            if (dpd == null) {
                dpd = DatePickerDialog.newInstance(
                    calendarType,
                    container.getContext(),
                    DatePickerFraction.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
                );
            } else {
                dpd.setCalendarType(calendarType);
                dpd.initialize(
                    DatePickerFraction.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
                );

            /**
             * 字体设置
             *
            switch (calendarType) {
                          case GREGORIAN:
                              dpd.setFont(null);
                              break;
          
                          case JALALI:
                              dpd.setFont(Utils.getFont(getFractionAbility(), "resources/rawfile/fonts/" + "IRANSansMobile(FaNum).ttf"););
                              break;
                      }
            */

            /**
             * 显示日期选择器
             */
             dpd.show();
        }
    }
    private OnDateSetListener mCallBack;
    if (mCallBack != null) {
            mCallBack.onDateSet(DatePickerDialog.this, mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        }
}
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
[Apache License, Version 2.0](LICENSE)
