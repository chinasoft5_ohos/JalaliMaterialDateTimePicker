package com.wdullaer.datetimepickerexampleohos;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class MainAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setPattern(ResourceTable.Pattern_base);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
