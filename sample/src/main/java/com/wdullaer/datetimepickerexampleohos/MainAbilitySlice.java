package com.wdullaer.datetimepickerexampleohos;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;
import ohos.agp.components.Text;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * News list slice
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = "sample22";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 1234567, TAG);

    PickerAdapter adapter;
    private Text mTv;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_main);

        adapter = new PickerAdapter();
        TabList tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        for (int i = 0; i < adapter.getCount(); i++) {
            TabList.Tab tab = tabList.new Tab(this);
            tab.setText(adapter.getTitle(i));
            tabList.addTab(tab);
        }
        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                selectPage(tab.getPosition());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
            }

            @Override
            public void onReselected(TabList.Tab tab) {
            }
        });
        tabList.selectTabAt(0);
    }

    private void selectPage(int i) {
        ((FractionAbility) getAbility()).getFractionManager()
            .startFractionScheduler()
            .replace(ResourceTable.Id_page_container, adapter.getItem(i))
            .submit();
    }

    private static class PickerAdapter {
        private static final int NUM_PAGES = 2;
        Fraction timePickerFraction;
        Fraction datePickerFraction;

        PickerAdapter() {
            timePickerFraction = new TimePickerFraction();
            datePickerFraction = new DatePickerFraction();
        }

        public int getCount() {
            return NUM_PAGES;
        }

        public Fraction getItem(int position) {
            switch (position) {
                case 0:
                    return datePickerFraction;
                case 1:
                default:
                    return timePickerFraction;
            }
        }

        int getTitle(int position) {
            switch (position) {
                case 0:
                    return ResourceTable.String_tab_title_date;
                case 1:
                default:
                    return ResourceTable.String_tab_title_time;
            }
        }
    }
}

