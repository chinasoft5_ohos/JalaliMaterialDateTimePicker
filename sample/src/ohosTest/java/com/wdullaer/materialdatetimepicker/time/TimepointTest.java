package com.wdullaer.materialdatetimepicker.time;

import static org.junit.Assert.*;

import ohos.utils.Parcel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Test for Timepoint which need to run on an actual device
 * Created by wdullaer on 1/11/17.
 */
@RunWith(JUnit4.class)
public class TimepointTest {
    @Test
    public void shouldCorrectlySaveAndRestoreAParcel() {
        Timepoint input = new Timepoint(1, 2, 3);
        Parcel timepointParcel = Parcel.create();
        input.marshalling(timepointParcel);

        Timepoint output = Timepoint.PRODUCER.createFromParcel(timepointParcel);
        assertEquals(input.getHour(), output.getHour());
        assertEquals(input.getMinute(), output.getMinute());
        assertEquals(input.getSecond(), output.getSecond());
    }
}
