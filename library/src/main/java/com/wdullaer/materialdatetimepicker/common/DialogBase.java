/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.common;

import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.KeyEvent;

public class DialogBase extends CommonDialog implements DialogInterface {
    private static final String TAG = "MonthFragment";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 1234567, TAG);

    private OnCancelListener onCancelListener;
    private OnDismissListener onDismissListener;
    private boolean cancelable = true;
    protected Context context;

    public DialogBase(Context context) {
        super(context);
        this.context = context;
        super.setDestroyedListener(() -> {
            if (onDismissListener != null) {
                onDismissListener.onDismiss(this);
            }
        });
    }

    @Override
    public boolean clickKeyUp(KeyEvent event) {
        HiLog.warn(LABEL, "### CancelableDialog clickKeyUp");
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEY_BACK) {
            HiLog.warn(LABEL, "### CancelableDialog KEY_BACK");
            if (cancelable) {
                cancel();
            } else {
                return true;
            }
        }
        return super.clickKeyUp(event);
    }

    @Override
    public void cancel() {
        HiLog.warn(LABEL, "### CancelableDialog cancel");
        if (onCancelListener != null) {
            onCancelListener.onCancel(this);
        }
        remove();
    }

    @Override
    public void dismiss() {
        if (onDismissListener != null) {
            onDismissListener.onDismiss(this);
        }
        remove();
    }

    public void setOnCancelListener(OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    public boolean isCancelable() {
        return cancelable;
    }
}