/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.common;

import ohos.agp.animation.AnimatorValue;

import java.util.ArrayList;
import java.util.List;

public class AnimatorValue2 extends AnimatorValue {
    List<ValueUpdateListener> mUpdateListeners;

    private String name;

    public String getName() {
        return name;
    }

    public AnimatorValue2 setName(String name) {
        this.name = name;
        return this;
    }

    public AnimatorValue2() {
        super();
        setValueUpdateListener(new ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (mUpdateListeners == null) {
                    return;
                }
                for (int i = 0; i < mUpdateListeners.size(); i++) {
                    ValueUpdateListener listener = mUpdateListeners.get(i);
                    if (listener != null) {
                        listener.onUpdate(animatorValue, v);
                    }
                }
            }
        });
    }

    public void addUpdateListener(ValueUpdateListener listener) {
        if (mUpdateListeners == null) {
            mUpdateListeners = new ArrayList<ValueUpdateListener>();
        }
        mUpdateListeners.add(listener);
    }

    public void removeUpdateListener(ValueUpdateListener listener) {
        if (mUpdateListeners == null) {
            return;
        }
        mUpdateListeners.remove(listener);
        if (mUpdateListeners.size() == 0) {
            mUpdateListeners = null;
        }
    }

    public void removeAllUpdateListeners() {
        if (mUpdateListeners == null) {
            return;
        }
        mUpdateListeners.clear();
        mUpdateListeners = null;
    }
}
