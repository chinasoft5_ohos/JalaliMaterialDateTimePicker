/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.common;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;

import com.wdullaer.materialdatetimepicker.Utils;

public class ResourceUtils {

    public static Color getColor(Context context, int id) {
        return getColor(context.getResourceManager(), id);
    }

    public static Color getColor(AttrSet attr, int id) {
        return attr.getAttr(id).get().getColorValue();
    }

    public static Color getColor(ResourceManager resourceManager, int id) {
        int color = 0;
        try {
            color = resourceManager.getElement(id).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Color(color);
    }

    public static int getColorValue(Context context, int id) {
        return getColorValue(context.getResourceManager(), id);
    }

    public static int getColorValue(ResourceManager resourceManager, int id) {
        int color = 0;
        try {
            color = resourceManager.getElement(id).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return color;
    }

    public static String getString(ResourceManager resourceManager, int id) {
        try {
            return resourceManager.getElement(id).getString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void setBackgroundColorRes(ResourceManager resourceManager, Component component, int colorResId) {
        Color color = getColor(resourceManager, colorResId);
        setBackgroundColor(component, color);
    }

    public static void setBackgroundColor(Component component, Color color) {
        ShapeElement bgColorElement = new ShapeElement();
        bgColorElement.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        component.setBackground(bgColorElement);
    }

    public static void setBackgroundColor(Component component, int color) {
        ShapeElement bgColorElement = new ShapeElement();
        bgColorElement.setRgbColor(RgbColor.fromArgbInt(color));
        component.setBackground(bgColorElement);
    }

    public static float getFloat(ResourceManager rm, int id) {
        try {
            return rm.getElement(id).getFloat();
        } catch (Exception e) {
            e.printStackTrace();
            return 0f;
        }
    }

    public static void printChilds(Component c, String indent) {
        if (c instanceof ComponentContainer) {
            Utils.log(indent + "container:" + c.getClass().getSimpleName() + ", rect: " + c.getComponentPosition());
            for (int i = 0; i < ((ComponentContainer) c).getChildCount(); i++) {
                printChilds(((ComponentContainer) c).getComponentAt(i), indent + "  |—");
            }
        } else {
            String text = c instanceof Text ? ((Text) c).getText() : "";
            Utils.log(indent + "child:" + c.getClass().getSimpleName() + ", rect: " + c.getComponentPosition() + ", " + text);
        }
    }

    public static int getDimensionPixelSize(Context context, int id) {
        ResourceManager resourceManager = context.getResourceManager();
        try {
            float value = resourceManager.getElement(id).getFloat();
            return Float.valueOf(value).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getDimensionPixelOffset(Context context, int id) {
        ResourceManager resourceManager = context.getResourceManager();
        try {
            float value = resourceManager.getElement(id).getFloat();
            return Float.valueOf(value).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
