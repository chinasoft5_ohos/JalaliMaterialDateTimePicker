/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.common;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Log {
    public static void d(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 1234567, tag);
        HiLog.debug(label, msg);
    }

    public static void i(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 1234567, tag);
        HiLog.info(label, msg);
    }

    public static void w(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 1234567, tag);
        HiLog.warn(label, msg);
    }

    public static void e(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 1234567, tag);
        HiLog.error(label, msg);
    }
}
