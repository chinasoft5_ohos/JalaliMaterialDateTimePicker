package com.wdullaer.materialdatetimepicker;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.IDataAbilityObserver;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.miscservices.timeutility.Time;
import ohos.sysappcomponents.settings.SystemSettings;
import ohos.utils.net.Uri;
import ohos.vibrator.agent.VibratorAgent;

import java.util.List;

/**
 * A simple utility class to handle haptic feedback.
 */
public class HapticFeedbackController {
    private static final int VIBRATE_DELAY_MS = 125;
    private static final int VIBRATE_LENGTH_MS = 50;
    private Integer mVibratorId;

    private static boolean checkGlobalSetting(Context context) {
        return SystemSettings.getValue(DataAbilityHelper.creator(context),
            SystemSettings.Sound.HAPTIC_FEEDBACK_STATUS) == "1";
    }

    private final Context mContext;
    private final IDataAbilityObserver mContentObserver;

    private VibratorAgent mVibratorAgent;
    private boolean mIsGloballyEnabled;
    private long mLastVibrate;

    public HapticFeedbackController(Context context) {
        mContext = context;
        mContentObserver = new IDataAbilityObserver() {
            @Override
            public void onChange() {
                mIsGloballyEnabled = checkGlobalSetting(mContext);
            }
        };
    }

    /**
     * Call to setup the controller.
     */
    public void start() {
        mVibratorAgent = new VibratorAgent();
        // 查询硬件设备上的振动器列表
        List<Integer> vibratorList = mVibratorAgent.getVibratorIdList();
        if (!vibratorList.isEmpty()) {
            mVibratorId = vibratorList.get(0);
        }

        // Setup a listener for changes in haptic feedback settings
        mIsGloballyEnabled = checkGlobalSetting(mContext);
        Uri uri = SystemSettings.getUri(SystemSettings.Sound.HAPTIC_FEEDBACK_STATUS);
        DataAbilityHelper.creator(mContext).registerObserver(uri, mContentObserver);
    }

    /**
     * Method to verify that vibrate permission has been granted.
     *
     * @param context
     * @return true if Vibrate permission has been granted
     */
    private boolean hasVibratePermission(Context context) {
        int hasPerm = context.verifyCallingPermission("ohos.permission.VIBRATE");
        return hasPerm == IBundleManager.PERMISSION_GRANTED;
    }

    /**
     * Call this when you don't need the controller anymore.
     */
    public void stop() {
        mVibratorAgent = null;
        Uri uri = SystemSettings.getUri(SystemSettings.Sound.HAPTIC_FEEDBACK_STATUS);
        DataAbilityHelper.creator(mContext).unregisterObserver(uri, mContentObserver);
    }

    /**
     * Try to vibrate. To prevent this becoming a single continuous vibration, nothing will
     * happen if we have vibrated very recently.
     */
    public void tryVibrate() {
        if (mVibratorAgent!=null){
            mVibratorAgent.startOnce(mVibratorId, VIBRATE_LENGTH_MS); // TODO 处理震动反馈，以下未执行
        }
        if (mVibratorAgent != null && mIsGloballyEnabled) {
            long now = Time.getRealActiveTime();
            // We want to try to vibrate each individual tick discretely.
            if (now - mLastVibrate >= VIBRATE_DELAY_MS) {
                mLastVibrate = now;
            }
        }
    }
}
