package com.wdullaer.materialdatetimepicker.date;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.wdullaer.materialdatetimepicker.ResourceTable;
import com.wdullaer.materialdatetimepicker.Utils;
import com.wdullaer.materialdatetimepicker.common.ResourceUtils;

public class DayPickerGroup extends StackLayout
    implements Component.ClickedListener, DayPickerView.OnPageListener {
    private Button prevButton;
    private Button nextButton;
    private DayPickerView dayPickerView;
    private DatePickerController controller;
    private Font font;

    public DayPickerGroup(Context context) {
        super(context);
        init();
    }

    public DayPickerGroup(Context context, /*@NonNull*/DatePickerController controller) {
        super(context);
        this.controller = controller;
        init();
    }

    public DayPickerGroup(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public DayPickerGroup(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init();
    }

    public DayPickerGroup(Context context, DatePickerController controller, Font font) {
        super(context);
        this.controller = controller;
        this.font = font;
        init();
    }

    private void init() {
        dayPickerView = new SimpleDayPickerView(getContext(), controller,font);
        addComponent(dayPickerView);

        final LayoutScatter scatter = LayoutScatter.getInstance(getContext());
        final ComponentContainer content = (ComponentContainer) scatter.parse(ResourceTable.Layout_mdtp_daypicker_group, this, false);

        // Transfer all children from the content to this
        while (content.getChildCount() > 0) {
            final Component view = content.getComponentAt(0);
            content.removeComponentAt(0);
            addComponent(view);
        }

        prevButton = (Button) findComponentById(ResourceTable.Id_mdtp_previous_month_arrow);
        nextButton = (Button) findComponentById(ResourceTable.Id_mdtp_next_month_arrow);

        if (controller.getVersion() == DatePickerDialog.Version.VERSION_1) {
            int size = Utils.vpToPx(16f, getContext());
            prevButton.setMinHeight(size);
            prevButton.setMinWidth(size);
            nextButton.setMinHeight(size);
            nextButton.setMinWidth(size);
        }

        if (controller.isThemeDark()) {
            Color color = ResourceUtils.getColor(getResourceManager(), ResourceTable.Color_mdtp_date_picker_text_normal_dark_theme);
        }

        prevButton.setClickedListener(this);
        nextButton.setClickedListener(this);

        dayPickerView.setOnPageListener(this);

        setLayoutRefreshedListener(layoutRefreshedListener);
    }

    private void updateButtonVisibility(int position) {
        final boolean isHorizontal = controller.getScrollOrientation() == DatePickerDialog.ScrollOrientation.HORIZONTAL;
        final boolean hasPrev = position > 0;
        final boolean hasNext = position < (dayPickerView.getCount() - 1);
        prevButton.setVisibility(isHorizontal && hasPrev ? Component.VISIBLE : Component.INVISIBLE);
        nextButton.setVisibility(isHorizontal && hasNext ? Component.VISIBLE : Component.INVISIBLE);
    }

    public void onChange() {
        dayPickerView.onChange();
    }

    public void onDateChanged() {
        dayPickerView.onDateChanged();
    }

    public void postSetSelection(int position) {
        dayPickerView.postSetSelection(position);
    }

    public int getMostVisiblePosition() {
        return dayPickerView.getMostVisiblePosition();
    }

    LayoutRefreshedListener layoutRefreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            int left = component.getLeft();
            int top = component.getTop();
            int right = component.getRight();
            int bottom = component.getBottom();

            final Button leftButton;
            final Button rightButton;
            if (getLayoutDirection() == LayoutDirection.RTL) {
                leftButton = nextButton;
                rightButton = prevButton;
            } else {
                leftButton = prevButton;
                rightButton = nextButton;
            }

            final int topMargin = controller.getVersion() == DatePickerDialog.Version.VERSION_1
                ? 0
                : ResourceUtils.getDimensionPixelSize(getContext(), ResourceTable.Float_mdtp_date_picker_view_animator_padding_v2);
            final int width = right - left;
            final int height = bottom - top;
            dayPickerView.setComponentPosition(0, topMargin, 0 + width, topMargin + height);
            Utils.log("DayPickerGroup onRefreshed dayPickerView position: " + dayPickerView.getComponentPosition());

            Utils.log("DayPickerGroup onRefreshed dayPickerView getFirstVisibleItemPosition: " + dayPickerView.getFirstVisibleItemPosition());
            final SimpleMonthView monthView = (SimpleMonthView) dayPickerView.getComponentAt(dayPickerView.getFirstVisibleItemPosition());
            final int monthHeight = monthView.getMonthHeight();
            final int cellWidth = monthView.getCellWidth();
            final int edgePadding = monthView.getEdgePadding();

            // Vertically center the previous/next buttons within the month
            // header, horizontally center within the day cell.
            final int leftDW = leftButton.getWidth();
            final int leftDH = leftButton.getWidth();
            final int leftIconTop = topMargin + monthView.getPaddingTop() + (monthHeight - leftDH) / 2;
            final int leftIconLeft = edgePadding + (cellWidth - leftDW) / 2;
            leftButton.setComponentPosition(leftIconLeft, leftIconTop, leftIconLeft + leftDW, leftIconTop + leftDH);
            Utils.log("DayPickerGroup onRefreshed leftButton position: " + leftButton.getComponentPosition());

            final int rightDW = rightButton.getWidth();
            final int rightDH = rightButton.getHeight();
            final int rightIconTop = topMargin + monthView.getPaddingTop() + (monthHeight - rightDH) / 2;
            final int rightIconRight = width - edgePadding - (cellWidth - rightDW) / 2 - 2;
            rightButton.setComponentPosition(rightIconRight - rightDW, rightIconTop,
                rightIconRight, rightIconTop + rightDH);
            Utils.log("DayPickerGroup onRefreshed rightButton position: " + rightButton.getComponentPosition());
        }
    };

    @Override
    public void onPageChanged(int position) {
        updateButtonVisibility(position);
        dayPickerView.accessibilityAnnouncePageChanged();
    }

    @Override
    public void onClick(Component c) {
        int offset;
        if (nextButton == c) {
            offset = 1;
        } else if (prevButton == c) {
            offset = -1;
        } else {
            return;
        }
        int position = dayPickerView.getMostVisiblePosition() + offset;

        // updateButtonVisibility only triggers when a scroll is completed. So a user might
        // click the button when the animation is still ongoing potentially pushing the target
        // position outside of the bounds of the dayPickerView
        if (position >= 0 && position < dayPickerView.getCount()) {
            dayPickerView.scrollTo(position);
            updateButtonVisibility(position);
        }
    }

}
