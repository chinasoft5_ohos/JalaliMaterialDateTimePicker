/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.date;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

import com.wdullaer.materialdatetimepicker.Utils;

public class AccessibleDateAnimator extends StackLayout {
//    private long mDateMillis;

    /**
     * 访问日期动画
     *
     * @param context 上下文
     * @param attrs attrs
     */
    public AccessibleDateAnimator(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * 设置日期,米尔斯
     *
     * @param dateMillis 日期,米尔斯
     */
    public void setDateMillis(long dateMillis) {
//        mDateMillis = dateMillis;
    }
    @Override
    public void addComponent(Component component) {
        component.setLayoutConfig(new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT));
        super.addComponent(component);
    }

    AnimatorProperty mIncomingAnimation;
    AnimatorProperty mOutgoingAnimation;

    public void setIncomingAnimation(AnimatorProperty animatorProperty) {
        mIncomingAnimation = animatorProperty;
    }

    public void setOutgoingAnimation(AnimatorProperty animatorProperty) {
        mOutgoingAnimation = animatorProperty;
    }

    public AnimatorProperty getIncomingAnimation() {
        return mIncomingAnimation;
    }

    public AnimatorProperty getOutgoingAnimation() {
        return mOutgoingAnimation;
    }

    public void setCurrentIndex(int childIndex){
        showOnly(childIndex, true);
    }

    private void showOnly(int childIndex, boolean animate) {
        final int count = getChildCount();
        AnimatorProperty inAnimation = getIncomingAnimation();
        AnimatorProperty outAnimation = getOutgoingAnimation();
        for (int i = 0; i < count; i++) {
            final Component child = getComponentAt(i);
            if (i == childIndex) {
                child.setVisibility(Component.VISIBLE);
                if (animate && inAnimation != null) {
                    inAnimation.setTarget(child).start();
                }
                Utils.log("showOnly childIndex:"+i+", VISIBLE");
            } else {
                child.setVisibility(Component.HIDE);
                if (animate && outAnimation != null && child.getVisibility() == Component.VISIBLE) {
                    outAnimation.setTarget(child).start();
                }
                Utils.log("showOnly childIndex:"+i+", HIDE");
            }
        }
    }
}
