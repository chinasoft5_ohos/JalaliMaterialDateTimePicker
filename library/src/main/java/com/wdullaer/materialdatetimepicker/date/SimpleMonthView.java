/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.date;

import ohos.agp.components.AttrSet;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.wdullaer.materialdatetimepicker.Utils;

public class SimpleMonthView extends MonthView {

    private static final String TAG = "MonthFragment";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 1234567, TAG);

    public SimpleMonthView(Context context, AttrSet attr, DatePickerController controller, Font font) {
        super(context, attr, controller, font);
    }

    @Override
    public void drawMonthDay(Canvas canvas, int year, int month, int day,
                             int x, int y, int startX, int stopX, int startY, int stopY) {
        Utils.log("SimpleMonthView drawMonthDay mSelectedDay:" + mSelectedDay + ", year:" + year + ", month:" + month + ", day:" + day);
        if (mSelectedDay == day) {
            canvas.drawCircle(x, y - (MINI_DAY_NUMBER_TEXT_SIZE / 3), DAY_SELECTED_CIRCLE_SIZE,
                mSelectedCirclePaint);
        }

        if (isHighlighted(year, month, day) && mSelectedDay != day) {
            canvas.drawCircle(x, y + MINI_DAY_NUMBER_TEXT_SIZE - DAY_HIGHLIGHT_CIRCLE_MARGIN,
                DAY_HIGHLIGHT_CIRCLE_SIZE, mSelectedCirclePaint);
            mMonthNumPaint.setFont(Font.DEFAULT_BOLD);
            if (font != null) {
                mMonthNumPaint.setFont(font);
            } else {
                mMonthNumPaint.setFont(Font.DEFAULT_BOLD);
            }
        } else {
            if (font != null) {
                mMonthNumPaint.setFont(font);
            } else {
                mMonthNumPaint.setFont(Font.DEFAULT);
            }
        }

        // gray out the day number if it's outside the range.
        if (mController.isOutOfRange(year, month, day)) {
            mMonthNumPaint.setColor(mDisabledDayTextColor);
        } else if (mSelectedDay == day) {
            if (font != null) {
                mMonthNumPaint.setFont(font);
            } else {
                mMonthNumPaint.setFont(Font.DEFAULT_BOLD);
            }
            mMonthNumPaint.setColor(mSelectedDayTextColor);
        } else if (mHasToday && mToday == day) {
            mMonthNumPaint.setColor(mTodayNumberColor);
        } else {
            mMonthNumPaint.setColor(isHighlighted(year, month, day) ? mHighlightedDayTextColor : mDayTextColor);
        }
        canvas.drawText(mMonthNumPaint, String.format(mController.getLocale(), "%d", day), x, y);
        Utils.log("SimpleMonthView drawMonthDay text:" + String.format(mController.getLocale(), "%d", day) + ", x:" + x + ", y:" + y + ", height:" + getHeight());
    }

    @Override
    protected void initView() {
        mMonthTitlePaint = new Paint();
        if (mController.getVersion() == DatePickerDialog.Version.VERSION_1)
            mMonthTitlePaint.setFakeBoldText(true);
        mMonthTitlePaint.setAntiAlias(true);
        mMonthTitlePaint.setTextSize(MONTH_LABEL_TEXT_SIZE);
        if (font != null) {
            mMonthTitlePaint.setFont(font);
        }
        mMonthTitlePaint.setColor(mDayTextColor);
        mMonthTitlePaint.setTextAlign(TextAlignment.CENTER);
        mMonthTitlePaint.setStyle(Paint.Style.FILL_STYLE);

        mSelectedCirclePaint = new Paint();
        mSelectedCirclePaint.setFakeBoldText(true);
        mSelectedCirclePaint.setAntiAlias(true);
        mSelectedCirclePaint.setAlpha(255);
        mSelectedCirclePaint.setColor(mTodayNumberColor);
        mSelectedCirclePaint.setTextAlign(TextAlignment.CENTER);
        mSelectedCirclePaint.setStyle(Paint.Style.FILL_STYLE);

        mMonthDayLabelPaint = new Paint();
        mMonthDayLabelPaint.setAntiAlias(true);
        mMonthDayLabelPaint.setTextSize(MONTH_DAY_LABEL_TEXT_SIZE);
        mMonthDayLabelPaint.setColor(mMonthDayTextColor);
        if (font != null) {
            mMonthDayLabelPaint.setFont(font);
        }
        mMonthDayLabelPaint.setStyle(Paint.Style.FILL_STYLE);
        mMonthDayLabelPaint.setTextAlign(TextAlignment.CENTER);
        mMonthDayLabelPaint.setFakeBoldText(true);

        mMonthNumPaint = new Paint();
        mMonthNumPaint.setAntiAlias(true);
        mMonthNumPaint.setTextSize(MINI_DAY_NUMBER_TEXT_SIZE);
        mMonthNumPaint.setStyle(Paint.Style.FILL_STYLE);
        mMonthNumPaint.setTextAlign(TextAlignment.CENTER);
        mMonthNumPaint.setFakeBoldText(false);
        if (font != null) {
            mMonthNumPaint.setFont(font);
        }
    }
}
