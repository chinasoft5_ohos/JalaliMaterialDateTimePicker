/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.date;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.wdullaer.materialdatetimepicker.ResourceTable;
import com.wdullaer.materialdatetimepicker.common.ResourceUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * A text view which, when pressed or activated, displays a colored circle around the text.
 */
public class TextViewWithCircularIndicator extends Text {

    private static final String TAG = "MonthFragment";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 1234567, TAG);

    private static final int SELECTED_CIRCLE_ALPHA = 255;

    Paint mCirclePaint = new Paint();

    private Color mCircleColor;
    private final String mItemIsSelectedText;

    private boolean mDrawCircle;
    private Map<Integer, Integer> mTextColorMap;

    public TextViewWithCircularIndicator(Context context, AttrSet attrs) {
        super(context, attrs);
        mCircleColor = ResourceUtils.getColor(getResourceManager(), ResourceTable.Color_mdtp_accent_color);
        mItemIsSelectedText = ResourceUtils.getString(getResourceManager(), ResourceTable.String_mdtp_item_is_selected);

        init();
    }

    private void init() {
        mCirclePaint.setFakeBoldText(true);
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(mCircleColor);
        mCirclePaint.setTextAlign(TextAlignment.CENTER);
        mCirclePaint.setStyle(Paint.Style.FILL_STYLE);
        mCirclePaint.setAlpha(SELECTED_CIRCLE_ALPHA / 255f);
        addDrawTask(mDrawTask, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }

    public void setAccentColor(int color, boolean darkMode) {
        mCircleColor = new Color(color);
        mCirclePaint.setColor(mCircleColor);
        mTextColorMap = createTextColor(color, darkMode);
        setComponentStateChangedListener(new ComponentStateChangedListener() {
            @Override
            public void onComponentStateChanged(Component component, int i) {
                if (mTextColorMap.containsKey(i)) {
                    setTextColor(new Color(mTextColorMap.get(i)));
                } else {
                    setTextColor(new Color(mTextColorMap.get(0)));
                }
            }
        });
    }

    @Override
    public void setTextColor(Color color) {
        HiLog.warn(LABEL, "TextViewWithCircularIndicator setTextColor:" + color.toString());
        super.setTextColor(color);
    }

    /**
     * Programmatically set the color state list (see mdtp_date_picker_year_selector)
     *
     * @param accentColor pressed state text color
     * @param darkMode current theme mode
     * @return ColorStateList with pressed state
     */
    private Map<Integer, Integer> createTextColor(int accentColor, boolean darkMode) {
        Map<Integer, Integer> colorMap = new HashMap<>();
        colorMap.put(ComponentState.COMPONENT_STATE_PRESSED, accentColor);
        colorMap.put(ComponentState.COMPONENT_STATE_SELECTED, Color.WHITE.getValue());
        colorMap.put(0, darkMode ? Color.WHITE.getValue() : Color.BLACK.getValue());
        return colorMap;
    }

    public void drawIndicator(boolean drawCircle) {
        mDrawCircle = drawCircle;
    }

    DrawTask mDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            HiLog.warn(LABEL, "TextViewWithCircularIndicator onDraw, mDrawCircle:" + mDrawCircle);
            if (mDrawCircle) {
                final int width = getWidth();
                final int height = getHeight();
                int radius = Math.min(width, height) / 2;
                canvas.drawCircle((float) width / 2, (float) height / 2,(float) radius, mCirclePaint);
            }
            setSelected(mDrawCircle);
        }
    };

    @Override
    public CharSequence getComponentDescription() {
        CharSequence itemText = getText();
        if (mDrawCircle) {
            return String.format(mItemIsSelectedText, itemText);
        } else {
            return itemText;
        }
    }
}
