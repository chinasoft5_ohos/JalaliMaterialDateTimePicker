/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.date;

import ohos.agp.text.Font;
import ohos.app.Context;

/**
 * An adapter for a list of {@link SimpleMonthView} items.
 *
 * @date 2021/07/30
 */
public class SimpleMonthAdapter extends MonthAdapter {

    public SimpleMonthAdapter(DatePickerController controller, Font font) {
        super(controller, font);
    }

    @Override
    public MonthView createMonthView(Context context, Font font) {
        return new SimpleMonthView(context, null, mController, font);
    }
}
