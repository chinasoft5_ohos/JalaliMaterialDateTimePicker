/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wdullaer.materialdatetimepicker.time;

import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;

import com.wdullaer.materialdatetimepicker.ResourceTable;
import com.wdullaer.materialdatetimepicker.common.Log;
import com.wdullaer.materialdatetimepicker.common.ResourceUtils;

/**
 * Draws a simple white circle on which the numbers will be drawn.
 */
public class CircleView extends Component implements Component.DrawTask {
    private static final String TAG = "CircleView";

    private final Paint mPaint = new Paint();
    private boolean mIs24HourMode;
    private int mCircleColor;
    private int mDotColor;
    private float mCircleRadiusMultiplier;
    private float mAmPmCircleRadiusMultiplier;
    private boolean mIsInitialized;

    private boolean mDrawValuesReady;
    private int mXCenter;
    private int mYCenter;
    private int mCircleRadius;

    public CircleView(Context context) {
        super(context);

        mIsInitialized = false;
    }

    public void initialize(Context context, TimePickerController controller) {
        if (mIsInitialized) {
            Log.e(TAG, "CircleView may only be initialized once.");
            return;
        }

        ResourceManager res = context.getResourceManager();

        int colorRes = controller.isThemeDark() ? ResourceTable.Color_mdtp_circle_background_dark_theme : ResourceTable.Color_mdtp_circle_color;
        mCircleColor = ResourceUtils.getColorValue(res, colorRes);
        mDotColor = controller.getAccentColor();
        mPaint.setAntiAlias(true);

        mIs24HourMode = controller.is24HourMode();
        if (mIs24HourMode || controller.getVersion() != TimePickerDialog.Version.VERSION_1) {
            mCircleRadiusMultiplier = ResourceUtils.getFloat(res, ResourceTable.Float_mdtp_circle_radius_multiplier_24HourMode);
        } else {
            mCircleRadiusMultiplier = ResourceUtils.getFloat(res, ResourceTable.Float_mdtp_circle_radius_multiplier);
            mAmPmCircleRadiusMultiplier = ResourceUtils.getFloat(res, ResourceTable.Float_mdtp_ampm_circle_radius_multiplier);
        }

        addDrawTask(this);

        mIsInitialized = true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int viewWidth = getWidth();
        if (viewWidth == 0 || !mIsInitialized) {
            return;
        }

        if (!mDrawValuesReady) {
            mXCenter = getWidth() / 2;
            mYCenter = getHeight() / 2;
            mCircleRadius = (int) (Math.min(mXCenter, mYCenter) * mCircleRadiusMultiplier);

            if (!mIs24HourMode) {
                // We'll need to draw the AM/PM circles, so the main circle will need to have
                // a slightly higher center. To keep the entire view centered vertically, we'll
                // have to push it up by half the radius of the AM/PM circles.
                int amPmCircleRadius = (int) (mCircleRadius * mAmPmCircleRadiusMultiplier);
                mYCenter -= amPmCircleRadius * 0.75;
            }

            mDrawValuesReady = true;
        }

        // Draw the white circle.
        mPaint.setColor(new Color(mCircleColor));
        canvas.drawCircle(mXCenter, mYCenter, mCircleRadius, mPaint);

        // Draw a small black circle in the center.
        mPaint.setColor(new Color(mDotColor));
        canvas.drawCircle(mXCenter, mYCenter, 8, mPaint);
    }
}
